import { IsNotEmpty, IsNumber, IsPositive, IsArray } from 'class-validator';
class CreatedOrderItemDto {
  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  productId: number;

  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  amount: number;
}
export class CreateOrderDto {
  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  customerId: number;

  @IsArray()
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
